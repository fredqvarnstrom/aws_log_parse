# AWS Log Parser: S3 -> Lambda -> RDS/RedShift
=================================================================

1. Install pymysql to local directory
    
    pip install pymysql -t <current directory>


2. Compress all files in this directory into a single .zip file and upload to AWS lambda.

3. Launch AWS Lambda.
    
    NOTE: Need to adjust **Memory** and **Timeout** value!
    