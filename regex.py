# Fallback regex for some odd logfiles. Still gets the required data.
fallback_regex_string = '(.*) \
\[(.*)\] \
(\S+) \
"([A-Z]+) \
(.+) \
(HTTP[^"]+)" \
(\d+) \
(\S+) \
"([^"]+)" \
"([^"]+)" \
"([^"]+)" \
"([^"]+)" \
"([^"]+)" \
"(.+)"'

# Break up logs into useful components with big hairy regex.
regex_string = '(<\d+>)\
(.*) \
(cache-[a-z]+[0-9]+) \
(.*): \
(.*) \
\[(.*)\] \
(\S+) \
"([A-Z]+) \
(.+) \
(HTTP[^"]+)" \
(\d+) \
(\S+) \
"([^"]+)" \
"([^"]+)" \
"([^"]+)" \
"([^"]+)" \
"([^"]+)" \
"(.+)"'
