from __future__ import print_function
import json
import os
import boto
import glob
import time
import sys
from multiprocessing import Pool
import logging.config

logging.config.fileConfig("logging.ini")
logger = logging.getLogger("Parser")

# Add parent directory to python path
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))

import utils

with open('../config.json', 'r') as f:
    config = json.load(f)

# What day do we want to gather logs for?
log_day = '2017-05-17'

log_dir = 'log/'

local_logfiles = None
try:
    # Parse a specific file?
    local_logfiles = [sys.argv[1]]
    logger.debug("Parsing only {0}...".format(local_logfiles[0]))
    fetch = False
except IndexError:
    local_logfiles = glob.glob(log_dir + '*.log')
    logger.debug("Parsing {0} logs found in this dir...".format(len(local_logfiles)))
    fetch = True

"""
Give us the meaning!
Takes the regex matchall from the fastly logs and gives it order.
Example match:
('<134>',
  '2017-05-05T15:59:59Z',
  'cache-atl6242',
  'pantheon-fastly-logs[369328]',
  '2606:a000:4e86:ce00:e97a:6933:a376:b484',
  '05/May/2017:15:59:59 +0000',
  'www.ymcatriangle.org',
  'GET',
  '/sites/default/files/js/js_glejqZY8OZKeijflYol79vp7xMrU338XuocnECIGm1U.js',
  'HTTP/1.1',
  '200',
  '20890',
  'HIT, HIT',
  '0.000',
  'application/x-javascript',
  '100c0f4c-1655-484c-b6d7-459a2c894503',
  'live',
  '(null)')
"""

"""
Parse a matched line from the above regex.
Make it manageable to understand later.
"""


def parse_all_logs(_local_logfiles, threads=4):
    _start = time.time()
    logger.debug("Firing up multithreaded parser...")
    # This turns the laptop into a space heater...
    pool = Pool(threads)
    pool.map(utils.multi_parse_logfile, _local_logfiles)
    logger.debug("\n\nDONE!\n\n")
    logger.debug("Parsed and wrote {0} lines in {1} seconds.".format(len(_local_logfiles), time.time() - _start))


def map_get(s3_key):
    try:
        localname = str(s3_key.name.split('/')[1]).replace(':', '_')
        if localname in local_logfiles:
            logger.debug("Skipping {0}: already here".format(s3_key.name))
            return
        s3_key.get_contents_to_filename(log_dir + localname)
        return log_dir + localname
    except Exception as er:
        logger.exception('Failed to get remove log file({}): {}'.format(s3_key.name, er))


def get_logfiles(_bucket, log_datestamp=log_day):
    # Iterate on calls like this:
    # l = bucket.list(prefix='fe1/2017-05-18')
    _s3_keys = []
    logger.debug("Listing all logfiles for {0}".format(log_day))
    for prefix in ['fe1/', 'fe2/', 'fe3/', 'fe4/']:
        for k in _bucket.list(prefix + log_datestamp):
            _s3_keys.append(k)
    return _s3_keys


def multi_fetch_logfiles(_s3_keys, threads=4):
    pool = Pool(threads)
    pool.map(map_get, _s3_keys)


if __name__ == '__main__':

    # Use BOTO to get logs from S3
    if fetch:
        logger.debug("Fetching logs for {0} from S3".format(log_day))
        try:
            start = time.time()
            s3 = boto.connect_s3(config.get('aws').get('access'),
                                 config.get('aws').get('secret'))
            logger.debug("Connecting to S3 to fetch logs...\n")
            bucket = s3.get_bucket('pantheon-fastly-logs')
            s3_keys = get_logfiles(bucket)
            logger.debug("Firing up multithreaded log downloader.")

            for _key in s3_keys:
                f_name = map_get(_key)
                if f_name is not None:
                    utils.multi_parse_logfile(f_name)
        except Exception as e:
            logger.exception("Couldn't connect to S3 to fetch logs: {}".format(e))
            exit(-1)

    # parse_all_logs(local_logfiles)
