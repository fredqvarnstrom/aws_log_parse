from __future__ import print_function
import datetime
import json
import os
import time
import re
import regex
import pymysql


conf_path = 'config.json'
if not os.path.isfile(conf_path):       # For the local parser...
    conf_path = '../config.json'

with open(conf_path, 'r') as f:
    config = json.load(f)


def lp(l):
    # Handle different dict lengths...
    if len(l) > 14:
        d = {'site': l[15],
             'env': l[16],
             'timestamp': datetime.datetime.strptime(l[1], '%Y-%m-%dT%H:%M:%SZ'),
             'request_ip': l[4],
             'hostname': l[6],
             'verb': l[7],
             'path': l[8],
             'response_code': l[10],
             'content': l[14],
             'php': 1 if l[17] == 'yes' else 0,
             'cached': 1 if 'HIT' in l[12] else 0,
             'backend_time_ms': int(float(l[13]) * 1000)
             }
    else:
        try:
            timestamp = datetime.datetime.strptime(l[1], '%d/%B/%Y:%H:%M:%S +0000')
        except ValueError:
            timestamp = datetime.datetime.strptime(l[1], '%d/%b/%Y:%H:%M:%S +0000')
        d = {'site': l[11],
             'env': l[12],
             'timestamp': timestamp,
             'request_ip': l[0],
             'hostname': l[2],
             'verb': l[3],
             'path': l[4],
             'response_code': l[6],
             'content': l[10],
             'php': 1 if l[9] == 'yes' else 0,
             'cached': 1 if 'HIT' in l[8] else 0,
             'backend_time_ms': int(float(l[9]) * 1000)
             }
    return d


# Coalesce hit/miss stats.
def hitmiss(pl, data=None):
    if data is None:
        data = {'total_hits': 0,
                'cached_hits': 0,
                'php_hits': 0,
                'start_time': datetime.datetime.now(),
                'end_time': datetime.datetime(2012, 1, 1),
                'site': pl['site'],
                'env': pl['env'],
                'response_code': pl['response_code']}
    data['total_hits'] = data['total_hits'] + 1
    if pl.get('cached'):
        data['cached_hits'] = data['cached_hits'] + 1
    if pl.get('php'):
        data['php_hits'] = data['php_hits'] + 1
    if pl.get('timestamp') < data['start_time']:
        data['start_time'] = pl.get('timestamp')
    if pl.get('timestamp') > data['end_time']:
        data['end_time'] = pl.get('timestamp')
    return data


# Count up unique IPs!
def ipcount(l, data=None):
    if data is None:
        data = {'day': datetime.date.today(),
                'site': l['site'],
                'env': l['env'],
                'request_ip': l['request_ip'],
                'requests': 0}
    data['requests'] = data['requests'] + 1
    return data


def db_get_conn():
    db_config = config.get('db')
    print('-- Connecting to db: {}'.format(db_config))
    try:
        conn = pymysql.connect(db_config['host'], user=db_config['user'], passwd=db_config['passwd'],
                               db=db_config['db'], connect_timeout=5)
        return conn
    except Exception as e:
        print('-- Failed to connect to mysql db: {}'.format(e))
        return None


# TODO: optimize
def db_write_raw_logs(lines):
    conn = db_get_conn()
    if conn is not None:
        x = conn.cursor()
        columns = lines[0].keys()
        sql = """INSERT INTO raw_logs (""" + ', '.join(columns) + """) \
              VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""

        print("-- Writing {0} rows".format(len(lines)))
        # import pdb;pdb.set_trace()
        x.executemany(sql, (l.values() for l in lines))
        conn.commit()
        x.close()
        conn.close()


def db_write_hit_counts(lines, conn, x):
    mapper = {}
    k = 0
    for l in lines:
        try:
            k = l.get('site') + l.get('env') + l.get('response_code')
            mapper[k] = hitmiss(l, mapper.get(k, None))
        except:
            pass
    columns = mapper[k].keys()
    sql = """INSERT INTO hits_rollup (""" + ', '.join(columns) + """) \
          VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"""

    # fix timestamps:
    for k, v in mapper.iteritems():
        mapper[k]['start_time'] = v['start_time'].strftime('%Y-%m-%d %H:%M:%S')
        mapper[k]['end_time'] = v['end_time'].strftime('%Y-%m-%d %H:%M:%S')

    x.executemany(sql, (m.values() for m in mapper.values()))
    conn.commit()


# Call this from the main loop and pass in a big list of parsed loglines.
def db_write_ip_counts(lines, conn, x):
    mapper = {}
    start = time.time()
    today = datetime.date.today()
    for l in lines:
        if l.get('php') == 1:
            k = today.isoformat() + l.get('site', '') + l.get('env', '') + l.get('request_ip', '')
            mapper[k] = ipcount(l, mapper.get(k, None))
    sql = """INSERT IGNORE INTO ips_rollup (site, env, day, request_ip, requests) \
          VALUES (%s, %s, %s, %s, %s)ON DUPLICATE KEY UPDATE
          `requests` = `requests` + %s"""

    # Add additional requests at the end and insure correct order
    rows = []
    for k, v in mapper.iteritems():
        rows.append([v['site'],
                     v['env'],
                     v['day'].isoformat(),
                     v['request_ip'],
                     v['requests'],
                     v['requests']
                     ]
                    )
        # Try writing smaller numbers to avoid deadlocks.
        x.executemany(sql, rows)
        conn.commit()
        rows = []

    print("Time to count unique IPs: {0}".format(time.time() - start))


def multi_parse_logfile(filename):
    fail_lines = 0
    lines = []
    # p = re.compile(regex.regex_string)
    fb = re.compile(regex.fallback_regex_string)
    conn = db_get_conn()
    x = conn.cursor()
    with open(filename, 'r') as _f:
        s_time = time.time()
        i = 0
        for i, line in enumerate(_f):
            l = fb.findall(line)
            if len(l) == 0:
                # print line
                fail_lines = fail_lines + 1
                continue
            pl = lp(l[0])
            if pl['site'] == '(null)':
                # Ignore requests w/o proper metadata
                continue
            lines.append(pl)
        if len(lines) == 0:
            print("?? Weirdly looks like {0} was empty!".format(filename))
            return
        db_write_hit_counts(lines, conn, x)
        db_write_ip_counts(lines, conn, x)
        print("-- PID {0}: Parsed {1} lines - {2}% failure rate. Time taken: {3}".format(os.getpid(), i,
                                                                                         fail_lines / i * 100,
                                                                                         time.time() - s_time))
        # db_write_raw_logs(lines)
    os.remove(filename)


def parse_log_content(content):
    fail_lines = 0
    lines = []
    fb = re.compile(regex.fallback_regex_string)
    conn = db_get_conn()
    if conn is not None:
        x = conn.cursor()
        s_time = time.time()
        print('-- Beginning of data: {}'.format(content[:30]))
        for line in content.splitlines():
            try:
                l = fb.findall(line)
                if len(l) == 0:
                    # print line
                    fail_lines = fail_lines + 1
                    continue
                pl = lp(l[0])
                if pl['site'] == '(null)':
                    # Ignore requests w/o proper metadata
                    continue
                lines.append(pl)
            except Exception as e:
                fail_lines += 1
                print('?? Failed to parse a line: {}'.format(line))
                print('?? Error msg: {}'.format(e))
        if len(lines) == 0:
            print("?? Weirdly looks like log file was empty!")
            return
        print('-- Parsing {} lines'.format(len(lines)))
        db_write_hit_counts(lines, conn, x)
        db_write_ip_counts(lines, conn, x)
        line_len = len(lines)
        print("-- PID {0}: Parsed {1} lines - {2}% failure rate. Time taken: {3}".format(os.getpid(), line_len,
                                                                                         fail_lines / line_len * 100,
                                                                                         time.time() - s_time))
        # db_write_raw_logs(lines)
